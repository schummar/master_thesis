#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "matrix.h"
#include "gauss.h"


static int id, size;
static int n;
static mat A;
static vec x, b, x_;

void init() {
  MPI_Comm_rank(MPI_COMM_WORLD, &id);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  if (id == 0) {
    A = mat_create(n, n);
    mat_fill(A);

    x = vec_create(n);
    vec_fill(x);

    b = vec_create(n);
    mat_mul(A, x, b);

    if (n <= 10) {
      mat_print(A, "A");
      vec_print(b, "b");
    }

    x_ = vec_create(n);
  }
}

void clean() {
  if (id == 0) {
    mat_delete(A);
    vec_delete(x);
    vec_delete(b);
    vec_delete(x_);
  }
}

void measure(void (*gauss)(const mat, const vec, vec)) {
  clock_t start = clock(), diff;
  printf("Solving linear equation system of dimension %i using %i processors.\n", n, size);


  gauss(A, b, x_);

  if (n <= 10) {
    vec_print(x, "x");
    vec_print(x_, "x'");
  }
  printf("Finished with ε = %g\n", vec_err(x, x_));

  diff = clock() - start;
  long msec = diff * 1000 / CLOCKS_PER_SEC;
  printf("Time :%ld_%ld ms\n", msec / 1000, msec % 1000);
}

mat Ap;

int main(int argc, char **argv) {

  MPI_Init(&argc, &argv);
  n = 10;
  if (argc > 1)
    n = atoi(argv[1]);
  srand(0);
  init();
  gauss_row_init(n);
  gauss_col_init(n);

  if (id == 0) {
    measure(&gauss_row);
    measure(&gauss_col);
    measure(&gauss_row);
    measure(&gauss_col);
  } else {
    gauss_row(0, 0, 0);
    gauss_col(0, 0, 0);
    gauss_row(0, 0, 0);
    gauss_col(0, 0, 0);
  }

  gauss_row_clean();
  gauss_col_clean();
  clean();
  MPI_Finalize();
  return 0;
}

