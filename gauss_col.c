#include <mpi.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

#include "matrix.h"

static int id, size; // mpi
static int n, m; // matrix size, number of rows for this process
static mat Ap;
static TYPE *Ap_data;
static vec bp, col;
static int *offs, *nums;

static MPI_Datatype scol_t, rcol_t;

void gauss_col_init(int n_) {
  MPI_Comm_rank(MPI_COMM_WORLD, &id);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  n = n_;
  m = n / size + (id < (n % size) ? 1 : 0);
  Ap = mat_create(n, m);
  Ap_data = *Ap;
  bp = vec_create(n);
  col = vec_create(n);

  if (id == 0) {
    offs = (int *) malloc(size * sizeof(int));
    nums = (int *) malloc(size * sizeof(int));
    int off = 0;
    for (int i = 0; i < size; i++) {
      offs[i] = off;
      nums[i] = n / size + (i < (n % size) ? 1 : 0);
      off += nums[i];
    }
  }

  MPI_Type_vector(n, 1, n, TYPE_MPI, &scol_t);
  MPI_Type_create_resized(scol_t, 0, sizeof(TYPE), &scol_t);
  MPI_Type_commit(&scol_t);
  MPI_Type_vector(n, 1, m, TYPE_MPI, &rcol_t);
  MPI_Type_create_resized(rcol_t, 0, sizeof(TYPE), &rcol_t);
  MPI_Type_commit(&rcol_t);
}

void gauss_col_clean() {
  mat_delete(Ap);
  vec_delete(bp);
  vec_delete(col);
  if (id == 0) {
    free(offs);
    free(nums);
  }
  MPI_Type_free(&scol_t);
  MPI_Type_free(&rcol_t);
}

static void spread(const mat A, const vec b) {
  mat_reset(Ap, n, m, Ap_data);
  MPI_Scatterv(id == 0 ? *A : 0, nums, offs, scol_t, *Ap, m, rcol_t, 0, MPI_COMM_WORLD);
  if (id == 0)
    vec_copy(bp, b, n);
  MPI_Bcast(bp, n, TYPE_MPI, 0, MPI_COMM_WORLD);
}

static void lu() {
  for (int k = 0, h = 0; k < n; k++) {
    int active = k % size;

    ///////////////////////////////////////////////////////////////////////////
    // find pivot element
    int pivot = -1;
    TYPE maxValue = -1;
    if (id == active) {
      for (int i = k; i < n; i++) { // (m-h) * 2Iop + Iop
        TYPE value = fabs(Ap[i][h]); // 2MEMR + FPAbs
        if (value > maxValue) { // FPComp
          pivot = i;
          maxValue = value;
        }
      }
    }
    MPI_Bcast(&pivot, 1, MPI_INT, active, MPI_COMM_WORLD);
    mat_perm(Ap, k, pivot);
    vec_perm(bp, k, pivot);


    ///////////////////////////////////////////////////////////////////////////
    // broadcast col
    if (id == active) {
      TYPE pivotValue = Ap[k][h];
      for (int i = k + 1; i < n; i++) {
        Ap[i][h] /= pivotValue;
        col[i] = Ap[i][h];
      }
      h++;
    }
    MPI_Bcast(col + k + 1, n - k - 1, TYPE_MPI, active, MPI_COMM_WORLD); // BCAST((n-k)*64)

    ///////////////////////////////////////////////////////////////////////////
    // 'real' work
    for (int i = k + 1; i < n; i++) { // (m-h) * 2IOp + IOp
      for (int j = h; j < m; j++) { // (n-k-1) * 2IOp + IOp
        Ap[i][j] -= Ap[k][j] * col[i]; // 2MEMR + MEMW + FPMul + FPAdd
      }
    }
  }
}

static void forward() {
  MPI_Request req;
  for (int k = 0, h = 0; k < n; k++) {
    int active = k % size, next = (k + 1) % size;

    if (id == active) {
      for (int i = k + 1; i < n; i++)
        bp[i] -= Ap[i][h] * bp[k];
      h++;
      MPI_Isend(bp, n, TYPE_MPI, next, 0, MPI_COMM_WORLD, &req);
    }
    else if (id == next) {
      MPI_Recv(bp, n, TYPE_MPI, active, 0, MPI_COMM_WORLD, 0);
    }
  }
}

static void backward(vec x) {
  MPI_Request req;
  for (int k = n - 1, h = m - 1; k >= 0; k--) {
    int active = k % size, next = (k + size - 1) % size;

    if (id == active) {
      bp[k] /= Ap[k][h];
      for (int i = 0; i < k; i++)
        bp[i] -= Ap[i][h] * bp[k];
      h--;
      MPI_Isend(bp, n, TYPE_MPI, next, 0, MPI_COMM_WORLD, &req);
    }
    else if (id == next) {
      MPI_Recv(bp, n, TYPE_MPI, active, 0, MPI_COMM_WORLD, 0);
    }
  }

  if (id == 0) {
    for (int i = 0; i < n; i++)
      x[i / size + offs[i % size]] = bp[i];
  }
}


void gauss_col(const mat A, const vec b, vec x) {
  spread(A, b);
  lu();
  forward();
  backward(x);
}

