## Echtzeitfähigkeit von parallelen Algorithmen zur Lösung von linearen Gleichungssystemen
### Masterarbeit von Marco Schumacher

Dieses Repository enthält die Implementierung der parallelen LU-Zerlegung, die im Rahmen der [Masterarbeit](https://bitbucket.org/schummar/master_thesis/downloads/marco_schumacher_masterarbeit.pdf) entstand.