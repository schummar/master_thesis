#include <mpi.h>
#include <stdlib.h>
#include <math.h>

#include "matrix.h"

static int id, size; // mpi
static int n, m; // matrix size, number of lines for this process
static mat Ap;
static TYPE *Ap_data;
static vec bp, row;
static int *pivot_process; // saves permutations,process that contains line
static int *offs, *nums;

static MPI_Datatype row_t;

void gauss_row_init(int n_) {
  MPI_Comm_rank(MPI_COMM_WORLD, &id);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  n = n_;
  m = n / size + (id < (n % size) ? 1 : 0);
  Ap = mat_create(m, n);
  Ap_data = *Ap;
  bp = vec_create(m);
  row = vec_create(n);
  pivot_process = (int *) malloc(n * sizeof(int));

  if (id == 0) {
    offs = (int *) malloc(size * sizeof(int));
    nums = (int *) malloc(size * sizeof(int));
    int off = 0;
    for (int i = 0; i < size; i++) {
      offs[i] = off;
      nums[i] = n / size + (i < (n % size) ? 1 : 0);
      off += nums[i];
    }
  }

  MPI_Type_vector(1, n, 0, TYPE_MPI, &row_t);
  MPI_Type_commit(&row_t);
}

void gauss_row_clean() {
  mat_delete(Ap);
  vec_delete(bp);
  vec_delete(row);
  free(pivot_process);
  if (id == 0) {
    free(offs);
    free(nums);
  }
  MPI_Type_free(&row_t);
}

static void spread(const mat A, const vec b) {
  mat_reset(Ap, m, n, Ap_data);
  MPI_Scatterv(id == 0 ? *A : 0, nums, offs, row_t, *Ap, m, row_t, 0, MPI_COMM_WORLD);
  MPI_Scatterv(b, nums, offs, TYPE_MPI, bp, m, TYPE_MPI, 0, MPI_COMM_WORLD);
}

static void lu() {
  for (int k = 0, h = 0; k < n; k++) {
    ///////////////////////////////////////////////////////////////////////////
    // find pivot element, first locally
    int max = -1;
    TYPE maxValue = -1;
    for (int i = h; i < m; i++) { // (m-h) * 2Iop + Iop
      TYPE value = fabs(Ap[i][k]); // 2MEMR + FPAbs
      if (value > maxValue) { // FPComp
        max = i;
        maxValue = value;
      }
    }

    ///////////////////////////////////////////////////////////////////////////
    // then globally
    struct {
      TYPE value;
      int id;
    } localMax, globalMax;
    localMax.value = maxValue;
    localMax.id = id;
    MPI_Allreduce(&localMax, &globalMax, 1, TYPE_MPI_INT, MPI_MAXLOC, MPI_COMM_WORLD); // ALLREDUCE(96)
    int active = globalMax.id;
    pivot_process[k] = active; // MEMW

    ///////////////////////////////////////////////////////////////////////////
    // broadcast current line
    vec line = id == active ? Ap[max] : row; // IOp + MEMR
    MPI_Bcast(line + k, n - k, TYPE_MPI, active, MPI_COMM_WORLD); // BCAST((n-k)*64)
    ///////////////////////////////////////////////////////////////////////////
    // reorder lines, save global position, increment unprogressed pointer
    if (id == active) { // Iop
      mat_perm(Ap, h, max); // 2MEMR + 2MEMW
      vec_perm(bp, h, max); // 2MEMR + 2MEMW
      h++;
    }

    ///////////////////////////////////////////////////////////////////////////
    // 'real' work
    for (int i = h; i < m; i++) { // (m-h) * 2IOp + IOp
      Ap[i][k] /= line[k]; // 3MEMR + MEMW + FPDiv
      for (int j = k + 1; j < n; j++) { // (n-k-1) * 2IOp + IOp
        Ap[i][j] -= Ap[i][k] * line[j]; // 2MEMR + MEMW + FPMul + FPAdd
      }
    }
  }
}

static void forward() {
  TYPE y_k;
  for (int k = 0, h = 0; k < n; k++) {
    if (id == pivot_process[k]) {
      y_k = bp[h];
      h++;
    }

    MPI_Bcast(&y_k, 1, TYPE_MPI, pivot_process[k], MPI_COMM_WORLD);

    for (int i = h; i < m; i++)
      bp[i] -= Ap[i][k] * y_k;
  }
}

static void backward(vec x) {
  TYPE x_k;
  for (int k = n - 1, h = m - 1; k >= 0; k--) {
    if (id == pivot_process[k]) {
      bp[h] /= Ap[h][k];
      x_k = bp[h];
      h--;
    }

    MPI_Bcast(&x_k, 1, TYPE_MPI, pivot_process[k], MPI_COMM_WORLD);

    for (int i = 0; i <= h; i++)
      bp[i] -= Ap[i][k] * x_k;

    if (id == 0) {
      x[k] = x_k;
    }
  }
}


void gauss_row(const mat A, const vec b, vec x) {
  spread(A, b);
  lu();
  forward();
  backward(x);
}

