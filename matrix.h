#ifndef MATRIX_H
#define MATRIX_H


typedef TYPE *vec;
vec vec_create(int n);
void vec_delete(vec v);
int vec_size(const vec v);
void vec_copy(vec dest, const vec src, int n);
void vec_fill(vec v);
void vec_print(const vec v, const char *name);
void vec_perm(vec v, int a, int b);
TYPE vec_err(const vec v, const vec w);


typedef vec *mat;
mat mat_create(int n, int m);
void mat_delete(mat A);
int mat_rows(const mat A);
int mat_cols(const mat A);
void mat_copy(mat dest, const mat src);
void mat_fill(mat A);
void mat_print(const mat A, const char *name);
void mat_perm(mat A, int a, int b);
void mat_reset(mat A, int n, int m, TYPE *data);
void mat_mul(const mat A, const vec x, vec b);

#endif
