#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>

#include "matrix.h"

typedef struct {
  void *pointer;
  int length;
} pointer_length_pair;
pointer_length_pair *list;
int count = 0, cap = 0;

void putLength(void *pointer, int length) {
  if (count == cap) {
    cap = (cap + 1) * 2;
    list = realloc(list, cap * sizeof(pointer_length_pair));
  }
  list[count].pointer = pointer;
  list[count].length = length;
  count++;
}

void removeLength(void *pointer) {
  for (int i = 0; i < count; i++) {
    if (list[i].pointer == pointer) {
      list[i] = list[count - 1];
      count--;
      break;
    }
  }
}

int getLength(void *pointer) {
  for (int i = 0; i < count; i++) {
    if (list[i].pointer == pointer)
      return list[i].length;
  }
  return -1;
}


void sappend(char **str1, int pad, const char *format, ...) {
  va_list ap;
  va_start(ap, format);

  size_t len1 = *str1 ? strlen(*str1) : 0;
  char *str2 = 0;
  int len2 = vasprintf(&str2, format, ap);


  pad -= len2;
  if (pad < 0) pad = 0;

  *str1 = (char *) (realloc(*str1, len1 + pad + len2 + 1));
  if (*str1 != 0) {
    for (int i = 0; i < pad; i++)
      (*str1)[len1 + i] = ' ';
    strcpy(&(*str1)[len1 + pad], str2);
  }
  free(str2);
  va_end(ap);
}

int min(int a, int b) {
  return a <= b ? a : b;
}

///////////////////////////////////////////////////////////////////////////////
// vector /////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

vec vec_create(int n) {
  vec v = (vec) malloc(n * sizeof(TYPE));
  putLength(v, n);
  return v;
}

void vec_delete(vec v) {
  removeLength(v);
  free(v);
}

int vec_size(const vec v) {
  return getLength(v);
}

void vec_copy(vec dest, const vec src, int n) {
  for (int i = 0; i < n; i++)
    dest[i] = src[i];
}

void vec_fill(vec v) {
  int n = vec_size(v);
  for (int i = 0; i < n; i++)
    v[i] = (TYPE) rand() / RAND_MAX;

}

void vec_print(const vec v, const char *name) {
  int n = vec_size(v);

  char *s = 0;

  if (name)
    sappend(&s, 0, "%s = ", name);
  sappend(&s, 0, "(");

  for (int i = 0; i < n; i++) {
    sappend(&s, 0, "%g", v[i]);
    if (i < n - 1) sappend(&s, 0, "  ");
  }

  sappend(&s, 0, ")\n");
  printf("%s", s);
}

void vec_perm(vec v, int a, int b) {
  TYPE t = v[a];
  v[a] = v[b];
  v[b] = t;
}

TYPE vec_err(const vec v, const vec w) {
  int vn = vec_size(v), wn = vec_size(w);
  if (vn != wn) {
    printf("verr: wrong dimensions");
    return -1;
  }

  TYPE e = 0;
  for (int i = 0; i < vn; i++) {
    TYPE t = fabs(v[i] - w[i]);
    if (t > e)e = t;
  }
  return e;
}


///////////////////////////////////////////////////////////////////////////////
// matrix /////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

mat mat_create(int n, int m) {
  mat A = (mat) malloc(n * sizeof(vec));
  putLength(A, n);

  if (n > 0) {
    TYPE *data = (TYPE *) malloc(n * m * sizeof(TYPE));
    for (int i = 0; i < n; i++) {
      A[i] = data + i * m;
      putLength(A[i], m);
    }
  }

  return A;
}

void mat_delete(mat A) {
  int n = mat_rows(A);

  if (n > 0) {
    vec min = A[0];
    for (int i = 1; i < n; i++) {
      if (A[i] < min)
        min = A[i];
      removeLength(A[i]);
    }
    free(min);
  }

  removeLength(A);
  free(A);
}

int mat_rows(const mat A) {
  return getLength(A);
}

int mat_cols(const mat A) {
  return getLength(A) ? getLength(A[0]) : 0;
}

void mat_copy(mat dest, const mat src) {
  int n = min(mat_rows(dest), mat_rows(src));
  int m = min(mat_cols(dest), mat_cols(src));
  for (int i = 0; i < n; i++)
    for (int j = 0; j < m; j++)
      dest[i][j] = src[i][j];
}

void mat_fill(mat A) {
  int n = mat_rows(A), m = mat_cols(A);
  for (int i = 0; i < n; i++)
    for (int j = 0; j < m; j++)
      A[i][j] = (TYPE) rand() / RAND_MAX;
}

void mat_print(const mat A, const char *name) {
  int n = mat_rows(A), m = mat_cols(A);

  char *s = 0;

  int l = name ? strlen(name) + 3 : 0;
  int coll[m];
  for (int j = 0; j < m; j++) {
    coll[j] = 0;
    for (int i = 0; i < n; i++) {
      int fieldl = snprintf(0, 0, "%g", A[i][j]);
      if (fieldl > coll[j]) coll[j] = fieldl;
    }
  }


  for (int i = 0; i < n; i++) {
    if (name && (i == n / 2))
      sappend(&s, 0, "%s = ", name);
    else sappend(&s, l, "");

    if (n == 1)
      sappend(&s, 0, "(");
    else if (i == 0)
      sappend(&s, 0, "/");
    else if (i == n - 1)
      sappend(&s, 0, "\\");
    else
      sappend(&s, 0, "|");

    for (int j = 0; j < m; j++) {
      sappend(&s, coll[j], "%g", A[i][j]);
      if (j < m - 1) sappend(&s, 0, "  ");
    }

    if (n == 1)
      sappend(&s, 0, ")");
    else if (i == 0)
      sappend(&s, 0, "\\");
    else if (i == n - 1)
      sappend(&s, 0, "/");
    else
      sappend(&s, 0, "|");

    sappend(&s, 0, "\n");
  }

  printf("%s", s);
}

void mat_perm(mat A, int a, int b) {
  vec t = A[a];
  A[a] = A[b];
  A[b] = t;
}

void mat_reset(mat A, int n, int m, TYPE *data) {
  for (int i = 0; i < n; i++, data += m)
    A[i] = data;
}

void mat_mul(const mat A, const vec x, vec b) {
  int An = mat_rows(A), Am = mat_cols(A);
  int xm = vec_size(x), bn = vec_size(b);

  if ((xm != Am) || (bn != An)) {
    printf("mmul: wrong dimensions!\n");
    return;
  }

  for (int i = 0; i < An; i++) {
    b[i] = 0;
    for (int j = 0; j < Am; j++)
      b[i] += A[i][j] * x[j];
  }
}
