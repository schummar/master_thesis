#ifndef GAUSS_H
#define GAUSS_H

#include "matrix.h"

void gauss_row_init(int n);
void gauss_row_clean();
void gauss_row(const mat A, const vec b, vec x);


void gauss_col_init(int n);
void gauss_col_clean();
void gauss_col(const mat A, const vec b, vec x);

#endif